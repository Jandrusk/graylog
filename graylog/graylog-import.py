#!/usr/bin/python

# Imports contents of all .gz compressed files in importPath ( with trailing / )
# to Graylog instance at graylogHost listening on graylogPort with a Raw/Plaintext TCP input
# Files that produce exceptions are renamed to filename.error, processed files are removed
# Attempts to acquire exclusive lock to lockPath and exits upon failure (safe to over schedule)
# Logs output to syslog
# Developed on Debian 9 Graylog/ES host using python 2.7.13 without additional packages

### Imports

from gzip import GzipFile as gzopen
import os
import sys
import socket
import fcntl
import syslog

### Vars

importPath = '/var/log/import/'
graylogHost = 'localhost'
graylogPort = 5555
lockPath = '/var/lock/graylog-importer.py'

### Code 

lockHandle = None

def fileLocked(filePath):
    global lockHandle 
    lockHandle= open(filePath, 'w')
    try:
        fcntl.lockf(lockHandle, fcntl.LOCK_EX | fcntl.LOCK_NB)
        return False
    except IOError:
        return True

if fileLocked(lockPath):
    syslog.syslog(syslog.LOG_ERR, 'Another instance is running exiting now')
    sys.exit(0)
else:
    syslog.syslog('Importing files')

    for file in os.listdir(importPath):
        if file.endswith(".gz"):

            s = socket.socket()
            s.connect((graylogHost,graylogPort))

            os.nice(10)

            try:
                with gzopen(importPath+file, "rb") as f:
                    for line in f:
                        #print "* "+line
                        s.sendall(line)
                os.remove(importPath+file)
            except:
                syslog.syslog(syslog.LOG_ERR, 'Error processing file ' + file)
                os.rename(importPath+file,  importPath+file+".error")

            s.close()

    os.remove(lockPath)
